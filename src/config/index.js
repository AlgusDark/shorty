import path from 'path';

function development(){
  return {
    app: {
      port: 3000,
      host: '127.0.0.1:3000',
      env: 'development'
    },
    db: {
      uri: 'mongodb://localhost:27017/wize'
    },
    jwt: {
      secret : 'b32f95a4-1d21-4182-b800-f487152133d1',
      expiresIn: '7d',
      aud: 'http://localhost',
      refreshTime: '21600' // 6h
    },
    winston: {
      options: {
        file: {
          level: 'info',
          filename: path.join(__dirname, '../storage/logs/app.log'),
          handleExceptions: true,
          json: true,
          maxsize: 5242880, //5MB
          maxFiles: 5,
          colorize: false
        },
        console: {
          level: 'debug',
          handleExceptions: true,
          json: false,
          colorize: true
        }
      }
    }
  }
}

function production(){
  return {
    app: {
      port: 3000,
      host: '127.0.0.1:3000',
      env: 'production'
    },
    db: {
      uri: 'mongodb://localhost:27017/wize'
    },
    jwt: {
      secret : 'b32f95a4-1d21-4182-b800-f487152133d1',
      expiresIn: '7d',
      aud: 'http://localhost',
      refreshTime: '21600' // 6h
    },
    winston: {
      options: {
        file: {
          level: 'info',
          filename: path.join(__dirname, '../storage/logs/app.log'),
          handleExceptions: true,
          json: true,
          maxsize: 5242880, //5MB
          maxFiles: 5,
          colorize: false
        },
        console: {
          level: 'debug',
          handleExceptions: true,
          json: false,
          colorize: true
        }
      }
    }
  }
}

const ENV = process.env.NODE_ENV || 'development';

const config = {
  'development' : development,
  'production' : production
}[ENV]();

export default config;