// IIFE - Immediately Invoked Function Expression
(function($, window, document) {
  var API = "http://localhost:3000/api/v1";

  $('.btn-shorten').on('click', function(){
    $.ajax({
      url: API+'/shorties',
      type: 'POST',
      dataType: 'JSON',
      data: {url: $('#url').val()},
      success: function(data){
        var shorty = data.host +"/"+ data.short
        var resultHTML = 'Your shorty url is <a class="result" href="' + shorty + '">'
            + shorty + '</a>';
        $('#link').html(resultHTML);
        $('#link').hide().fadeIn('slow');
      }
    });

  });
}(window.jQuery, window, document));