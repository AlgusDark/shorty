import Url from '../models/url';

function index(req, res, next) {
  return res.sendFile(path.join(__dirname, '../public/index.html'));
};

function redirect(req, res, next) {
  const short = req.params.short;

  Url.findOne({short}).then(url => {
    if(!url) res.json({message: 'not valid url'});
    return res.redirect(url.long);
  });
};

export default {
  index,
  redirect
}