import HttpError from '../errors/Http';
import User from '../models/user';
import jwt from '../utils/jwt';

function index(req, res, next) {
  if(res.locals.role != 'admin'){
    return next(new HttpError(401));
  }

  User.find({}).then(users => {
    const {token} = res.locals.token;
    return res.json({users, token});
  });
}

function show(req, res, next) {
  User.findById(req.params.id).then(user => {
    const {token} = res.locals.token;

    if (!user) return next(new HttpError(400, `There are no user with that criteria.`));

    return res.json({user, token});
  }).catch(err => next(new HttpError(400)));
}

function edit(req, res, next) {
  if(req.params.id != res.locals.id){
    return next(new HttpError(401));
  }

  User.findById(req.params.id).then(user => {
    const {email, oldPassword, newPassword} = req.body;

    if(oldPassword){
      user.verifyPassword(oldPassword).then(match => {
        if(!match) {
          return next(new HttpError(400, `Please verify your password`));
        }

        [user.email, user.password] = [email, newPassword];

        user.save()
          .then(user => saveUser(user, res))
          .catch(err => next(new HttpError(500)));
      })
    }else if(email){
      user.email = email;

      user.save()
        .then(user => saveUser(user, res))
        .catch(err => next(new HttpError(500, 'This email address is already taken, please try another')));
    }else{
      return next(new HttpError());
    }
  }).catch(err => next(new HttpError(400)));

}

function destroy(req, res, next) {
  if(req.params.id != res.locals.id && res.locals.role != 'admin'){
    return next(new HttpError(401));
  }

  const password = req.body.password;

  if(res.locals.role == 'admin'){
    User.findByIdAndRemove(req.params.id)
      .then(() => {
        return res.json({message: 'User successfully deleted.'});
      })
      .catch(() => next(new HttpError(400, `User doesn't exist`)));
  }else if(password){
    User.findById(req.params.id).then(user => {
      user.verifyPassword(password).then(match => {
        user.remove()
          .then(() => {
            return res.json({message: 'User successfully deleted.'});
          })
          .catch(err => next(new HttpError()));
      });
    });
  }else{
    return next(new HttpError());
  }
}


// Helpers
function saveUser(user, res){ 
  return res.json({token: jwt.signToken(user.id, user.role)});
}

export default {
  index,
  show,
  edit,
  destroy
}