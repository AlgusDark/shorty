import Url from '../models/url';
import HttpError from '../errors/Http';

function index(req, res, next) {
  Url.find({}).then(url => {
      return res.json({url});
    });
}

function save(req, res, next) {
  let long = req.body.url;

  if(!long.startsWith("http")){
    long = "http://" + long;
  }

  if(!long) return next(new HttpError(500, 'We need a url to do the magic!'));

  Url.findOne({ long }).then(url => {
    if (!url) {
      const url = new Url({long});

      url.save().then(url => {
        return res.json(url);
      }).catch(err => next(new HttpError(409, err)));
    }else{
      return res.json(url);
    }
  }).catch(err => next(new HttpError(500, err)));
}

function show(req, res, next) {
  const short = req.params.short;

  Url.findOne({ short }).then(url => {
    if (!url){
      return next(new HttpError(409, `That doesn't exist, please stop drinking`));
    }

    return res.json(url);
  })
}

export default {
  index,
  save,
  show
}