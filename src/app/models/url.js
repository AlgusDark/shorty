import mongoose from 'mongoose';
import config from '../../config';

const UrlSchema = new mongoose.Schema({
  long: {
    type: String,
    required: [true, 'URL is required'],
    trim: true,
    index: { unique: true },
    match: [
    /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,
    'Please fill a valid URL address'
    ]
  },
  short: {
    type: String,
    index: { unique: true }
  },
  host: {
    type: String,
    default: `http://${config.app.host}`
  }
});

UrlSchema.pre('save', hashUrl);

function hashUrl(next) {
  const url = this;

  if(!url.long.startsWith("http")){
    url.long = "http://" + url.long
  }

  url.short = shorty();

  url.constructor.findOne({short: url.short}).then(url => {
    if(!url){
      next(url);
    }else{
      hashUrl.call(url, next);
    }
  });
}

function shorty(){
  let short = '';
  
  const alphabet = Array.from(
    'abcdefghijklmnopqrstuvwxyz' +
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
    '0123456789'
    );

  const base = alphabet.length - 1;

  for (let i = 0; i < 8 ; i++) {
    short += alphabet[Math.floor(Math.random() * (base - 0) + 0)];
  }

  return short;
}

export default mongoose.model('Url', UrlSchema);