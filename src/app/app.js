import "babel-polyfill";

import path from 'path';

import config from '../config';

import logger from "./utils/logger";
import mongoose from './utils/mongoose';

import app from '../config/express';

import apiRoutes from './routes/api/v1';
// import authRoutes from './routes/auth';
import rootRoutes from './routes/';

import errorHandlingMiddleware from "./middlewares/errorHandling";

// Connect to Mongo Database
mongoose.connect();

// API Routes
app.use('/api/v1', apiRoutes);
// app.use('/auth', authRoutes);

// Root Routes
app.use('/', rootRoutes);

// Error Handling Middleware
app.use(errorHandlingMiddleware);

app.listen(config.app.port, () =>{
  logger.info(`Express server listening on port ${config.app.port} in ${app.get('env')}`)
});