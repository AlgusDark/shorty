import express from 'express';

// import authMiddleware from '../../../middlewares/auth';
import shortController from '../../../controllers/short';

const router = express.Router();

// route middleware
// router.use(authMiddleware);

// list all shortened urls
router.get('/shorties', shortController.index);

// Create a short URL given a long URL
router.post('/shorties', shortController.save);

// Retrieve long URL given a short url
router.get('/shorties/:short', shortController.show);

export default router;