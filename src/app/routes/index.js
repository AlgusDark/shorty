import express from 'express';

import rootController from '../controllers';

const router = express.Router();

// Create and return a shortened URL given a long URL
router.get('/', rootController.index);

// Redirect the visitor to their original URL given the short URL
router.get('/:short', rootController.redirect);

export default router;